Mathograpycs
============

A Python abstraction library to draw vectorial paths.

MIT licensed

General
----

Mathograpycs' name comes from the book [Mathographics](https://www.goodreads.com/book/show/1406653.Mathographics) by Robert Dixon. It has no connection with it besides the fact that I got the inspiration from trying to implement the computer drawings (and algorithms) described in such book.

The library is similar to the old Logo turtle. My aim was to have an intuitive way of drawing vectorial images without much knowledge of matrices. Yet it is possible to access this information because the library performs the calculation using matrices.

* The base item in a mathograpycs program is a Canvas.
* A canvas can create pens.
* A pen is a predefined set of options that influence the way the vectorial path is written on the medium.
* The medium is written by the emitter, which has concrete knowledge of how to write on the medium. 
* Currently only emission to SVG file is supported. I plan to release writing to other kinds of media, but I don't know when. 
* A canvas comes with an _arm_, which is essentially a state that tells where the position of the pen is.
* Pens can be changed but it is the arm that holds them, therefore the position information is not inherent the pen but the arm.
* One creates a path (a `Sequence`) by composing atomic actions with operators. 

### Operators

#### Move operators

* `>>` concatenate, pen down
* `<<` concatenate, pen up

#### Pen mode operators

* `pol()` absolute polar
* `cart()` absolute cartesian
* `rpol()` relative polar
* `rcart()` relative cartesian

#### Pen selection

* Any pen object passed as operand to a move operator.

#### Shapes

* `tuple` A sequence of coordinates; their interpretation depends from the last encountered mode
* `Spiral` A spiral
* `Polygon` A polygon

#### Misc

* `close()` Close the current path by dragging the pen to the point where it went down
* `o in canvas` draws the `Sequence` `o` in the `Canvas` `canvas`
* `newref()` Moves the reference system to the current position of the arm. The previous position is not forgotten (but right now there is a little bit of vagueness in how the reference system stack is to be accessed)
* `"a string"` If passed as operand to `>>` or `<<`, it remembers the position the arm has, which can be restored with the following command:
* `restore("a string")` restores the position of the arm when the last occurrence of `"a string"` was encountered

### Examples

#### cartesian coordinates; origin
By default the origin is always placed in the middle of the medium. (This is not definitive of course). In the following example the pair `ascissa` and `ordinata` show, besides the origin and the orientation of the axes, how one can switch from cartesian to relative cartesian coordinates.



_doc_sequence.py_

```python
from core import Canvas, cart, rcart, close
from emit.continuous import ContinuousPath
"""
Move the pen using cartesian coordinates.
"""
with Canvas(600,300, emitter=ContinuousPath(f'{__file__}.svg')) as canvas:
    black = canvas.pen(name="cross", stroke="black", stroke_width=1)
    blue = canvas.pen(name="first", stroke="blue", stroke_width=3)
    green = canvas.pen(name="small", stroke="green", stroke_width=3)

    s0 = blue << (100, 100) >> (100, -100, -100, -100, -100, 100)

    abscissa = cart() >> black << (0, 0) >> (0, 0, 100, 0) >> rcart() >> (-10, -10)
    ordinata = cart() >> black << (0, 0) >> (0, 0, 0, 100) >> rcart() >> (-10, -10)

    def square(x):
        return x >> (50,0, 0, 50, -50, 0) >> close() << (100, -100)
    s0 in canvas
    abscissa in canvas
    ordinata in canvas
    square(green) in canvas
    square(blue) in canvas

```

_doc_sequence.py.svg_

![alt_text](doc/doc_sequence.py.svg) 

 

Note that the order in which the sequences are added to the canvas with `in` matters.

#### polar coordinates ; debug mode

Polar coordinates are enabled when a `pol()` is encountered in the sequence. The tuples that follow will then be interpreted as pairs of modulo and angle in _degrees_.

`rpol()` is the relative version, where the angle is accrued from the position.

Angles are always evaluated with respect to the current reference system, marked by the black axes in the following drawing.



_doc_polar.py_

```python
from core import Canvas, cart, rcart, close, pol, rpol
from emit.continuous import ContinuousPath
"""
Move the pen using polar coordinates.
"""
with Canvas(600,300, emitter=ContinuousPath(f'{__file__}.svg',debug=True)) as canvas:
    black = canvas.pen(name="cross", stroke="black", stroke_width=1)
    blue = canvas.pen(name="first", stroke="blue", stroke_width=3)
    green = canvas.pen(name="small", stroke="green", stroke_width=3)

    s0 = blue >> pol() >> (100, 45, 100, 90, 100, 100)
    s1 = green >> rpol() >> (20, 20, 30, 20, 40, 20, 50,20)
    abscissa = cart() >> black << (0, 0) >> (0, 0, 100, 0) >> rcart() >> (-10, -10)
    ordinata = cart() >> black << (0, 0) >> (0, 0, 0, 100) >> rcart() >> (-10, -10)

    s0 in canvas
    s1 in canvas
    abscissa in canvas
    ordinata in canvas

```

_doc_polar.py.svg_

![alt_text](doc/doc_polar.py.svg) 



 The `debug=True` argument to the emitter specifies that, for each atomic movement performed by the arm, the orientation of the arm is drawn. 
 
 The definition of atomic movement depends on the chosen `Emitter`, and in this case (`ContinuousPath`) it is each processed Python `tuple`.
 
 Note that in this case the initial angle when tracing `s1` (in green) is the one the arm had when completing `s0` (100 degrees). Note how the 20 degrees accrue.


#### move reference system with `newref()`

`newref()` is similar to stacking the current position on a stack of transformations. The effect is that, when switching to absolute, the coordinates are interpreted as relative to the coordinate system that the arm had upon calling `newref()` 



_doc_switch.py_

```python
from core import Canvas, cart, rcart, close, pol, rpol, newref
from emit.continuous import ContinuousPath
"""
Switch coordinate system.
"""
with Canvas(600,300, emitter=ContinuousPath(f'{__file__}.svg',debug=True)) as canvas:

    black = canvas.pen(name="cross", stroke="black", stroke_width=1)
    blue = canvas.pen(name="first", stroke="blue", stroke_width=3)
    green = canvas.pen(name="small", stroke="green", stroke_width=3)

    abscissa = cart() >> black << (0, 0) >> (0, 0, 100, 0) >> rcart() >> (-10, -10)
    ordinata = cart() >> black << (0, 0) >> (0, 0, 0, 100) >> rcart() >> (-10, -10)
    move = cart() << (0, 0) << pol() << (100,30) >> newref()
    square =  blue >> cart() >> (50,0,50,50,0,50,0,0)

    abscissa in canvas
    ordinata in canvas
    move in canvas
    square in canvas
```

_doc_switch.py.svg_

![alt_text](doc/doc_switch.py.svg) 



The example shows how this library avoids the explicit mentioning of matrices.

1. The arm is moved to create the previously seen initial axes `abscissa` and `ordinata`.
2. the `move` sequence moves back to the origin, sets the mode to absolute polar, moves to `100, 30`
3. `square` specifies a square in absolute cartesian coordinates, which are interpreted in the reference system rotated by 30 degrees and translated by 100 units.

#### Shapes

It is possible to use some shorthands to create "polar curves". Polygons, stars and spirals are "polar curves", because they are generated by modulating the angle the pen is writing. 

I beg your pardon for my sloppiness: I am sure there is a more rigorous definition of this concept.



_doc_shapes.py_

```python
import math

from core import Canvas, cart, rcart, close, pol, rpol, newref, Spiral, Polygon
from emit.continuous import ContinuousPath
"""
Some shapes.
"""
with Canvas(600,300, emitter=ContinuousPath(f'{__file__}.svg',debug=False)) as canvas:
    black = canvas.pen(name="cross", stroke="black", stroke_width=1)
    blue = canvas.pen(name="first", stroke="blue", stroke_width=3)
    green = canvas.pen(name="small", stroke="green", stroke_width=3)
    its = 50
    poli = Polygon(10, 5, star=1)
    spi = Spiral(radius_fun=lambda r, a: .95 * math.sqrt(a),
             #divergence_fun=lambda r, a: a+10,
             divergence_fun=Spiral.daisy,
             of=poli,
             iterations=its,
             )

    spi_line = Spiral(radius_fun=lambda r, a: 5 * math.sqrt(a),
                  divergence_fun=lambda r, a: a + 10,
                  iterations=its,
                  )

    spiral = green >> pol() >> spi_line
    daisy = blue << pol() << (150, 15) >> newref() >> spi

    abscissa = cart() >> black << (0, 0) >> (0, 0, 100, 0) >> rcart() >> (-10, -10)
    ordinata = cart() >> black << (0, 0) >> (0, 0, 0, 100) >> rcart() >> (-10, -10)

    spiral in canvas
    abscissa in canvas
    ordinata in canvas
    daisy in canvas

```

_doc_shapes.py.svg_

![alt_text](doc/doc_shapes.py.svg) 



In this example two spirals are created. Spirals are defined by:

 * the number of `iterations`
 * the change in

    * radius `radius_fun` 
    * angle `divergence_fun` 
 
 of the pen at each iteration.
The spiral `spi` is a daisy, that is, a spiral with a special divergence function and made of pentagons (`of` argument).

The `star` argument of `Polygon` is a parameter that governs the behaviour of the algorithm tracing the polygon. It is called star because it determines if the result will be a regular polygon or a star, as the next example shows.



_doc_star.py_

```python
import math

from core import Canvas, cart, rcart, close, pol, rpol, newref, Spiral, Polygon
from emit.continuous import ContinuousPath
"""
Polygons and stars.
"""
with Canvas(600,300, emitter=ContinuousPath(f'{__file__}.svg',debug=False)) as canvas:
    black = canvas.pen(name="cross", stroke="black", stroke_width=1)
    blue = canvas.pen(name="first", stroke="blue", stroke_width=3)
    green = canvas.pen(name="small", stroke="green", stroke_width=3)
    p = pol()
    abscissa = cart() >> black << (0, 0) >> (0, 0, 100, 0) >> rcart() >> (-10, -10)
    ordinata = cart() >> black << (0, 0) >> (0, 0, 0, 100) >> rcart() >> (-10, -10)
    for c, x in zip([green, blue, black], range(1,4)):
        p = p >> c >> Polygon(20, 7, star=x) << cart() << (50,0) >> newref() >> pol()
    abscissa in canvas
    ordinata in canvas
    p in canvas


```

_doc_star.py.svg_

![alt_text](doc/doc_star.py.svg) 



#### save and restore 

You can save the position of a reference system by issuing
`>> "name of reference system"` or `<< "name of reference system"` in a sequence.

To recall it, `restore("name of reference system")`



_doc_save.py_

```python
from core import Canvas, cart, rcart, close, Polygon, pol, restore, newref
from emit.continuous import ContinuousPath
"""
Save position.
"""
with Canvas(600,300, emitter=ContinuousPath(f'{__file__}.svg', debug=True) ) as canvas:
    black = canvas.pen(name="cross", stroke="black", stroke_width=1)
    blue = canvas.pen(name="first", stroke="blue", stroke_width=3)
    green = canvas.pen(name="small", stroke="green", stroke_width=3)

    poly = Polygon(30,7,star=1)

    abscissa = cart() >> black << (0, 0) >> (0, 0, 100, 0) >> rcart() >> (-10, -10)
    ordinata = cart() >> black << (0, 0) >> (0, 0, 0, 100) >> rcart() >> (-10, -10)

    def shape(pen, center):
       return pen << center >> newref() >> poly >> restore("origin") >> "origin"

    complicated = pol() << (60,60) >> newref() >> "origin" >> \
                  shape(black, (70, -90)) >> \
                  shape(blue, (70,0)) >> \
                  shape(green, (70, 90))

    abscissa in canvas
    ordinata in canvas
    complicated in canvas

```

_doc_save.py.svg_

![alt_text](doc/doc_save.py.svg) 



The subsequence `restore("origin") >> "origin"` is currently a workaround to a bug -- it prevents the stored `origin` to be rewritten, by making a new copy of the internal transform stack after having it recalled. 

I am still wondering if this is something unwanted or if I made it like this on purpose.

### Conclusion, other emitters

This library is full of bugs, too many for the simple task it performs. 

I have been working a while on a different SVG emitter, `Define` (available in the source code), which uses svg definitions of entities. 

For small drawings it is anyway more verbose than `ContinuousPath`. I haven't thoroughly investigated yet what 'small' and 'more' mean. 

The output svg of the `Define` emitter requires a SVG 2.0 compliant SVG interpreter (Mozilla Firefox works fine).