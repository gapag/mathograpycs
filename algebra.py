import itertools
from collections import OrderedDict
from copy import copy
from math import degrees, radians, sin, cos, tan, asin, acos, atan, atan2, sqrt


class Coordinate:
    def __init__(self, v):
        self._v_ = v

    def __iter__(self):
        return iter(self._v_)


class Mat:
    """
    Matrix type.
    """

    def __eq__(self, o: object) -> bool:
        return self.rows == o.rows

    def __init__(self, angle=0, translation=(0, 0), scaling=(1, 1)):
        s, c = [f(radians(angle)) for f in [sin, cos]]
        x, y = map(float, translation)
        try:
            sx, sy = map(float, scaling)
        except:
            sx = sy = 1
        self.__degrees_angle = angle

        X = x
        Y = y

        self.rows = [
            [sx * c, -s * sy, X],
            [sx * s, sy * c, Y],
            [0, 0, 1]
        ]

    def __getitem__(self, item):
        return self.rows[item]
    
    def zero(self):
        return self.translation

    @classmethod
    def identity(cls):
        return Mat(0, (0, 0))

    def __translation(self):
        return self.rows[0][-1], self.rows[1][-1]

    def __angle(self):
        return self.__degrees_angle

    def __scaling(self):
        x = cos(radians(self.__degrees_angle))
        return self.rows[0][0] / x, self.rows[1][1] / x

    translation = property(fget=__translation)
    angle = property(fget=__angle)

    def __call__(self, *other, **kwargs):
        return self._transform(self.rows, *other)

    def __matmul__(self, other):
        if isinstance(other, Mat):
            return self._matrix_mult(self, other)
        else:
            return self._transform_one(self.rows, other)

    def _transform_one(self, matrix, x):
        # try:
        #     x = x.v
        # except:
        #     pass
        if len(x) == 2:
            x = tuple(x) + (1,)
        if len(x) != 3:
            raise ValueError
        A, B, C = x
        return tuple(round(m1 * A + m2 * B + m3, 6) for m1, m2, m3 in matrix)[:2]

    def _transform(self, matrix, *other):
        rsl = []
        for x in other:
            rsl.append(self._transform_one(matrix, x))
        return rsl

    def rot(self, *other):
        alt_rows = [(x, y, (z if idx == 2 else 0))
                    for idx, (x, y, z) in enumerate(self.rows)]
        return self._transform(alt_rows, *other)

    @classmethod
    def _matrix_mult(cls, a, b):
        def term(i, j):
            r = 0
            for ii, x in enumerate(a[i]):
                r += x * b[ii][j]
            return r

        ones = cls.identity()
        ones.rows = [[term(i, j) for j in range(0, 3)] for i in range(0, 3)]
        return ones

    # def __matmul__(self, other):
    #     if isinstance(other, Mat):
    #         return self._matrix_mult(self.rows, other.rows)
    #     # elif isinstance(other, Rectangle):
    #     #     raise NotImplementedError()
    #     else:
    #         return self(other)[0]
    def __str__(self):
        return (("{:<10f}" * 3 + "\n") * 3).format(
            *self.rows[0], *self.rows[1], *self.rows[2]
        )
    
    def __iter__(self):
        return iter ((self.rows[0][0], 
                    self.rows[1][0],
                    self.rows[0][1],
                    self.rows[1][1],
                    self.rows[0][2],
                    self.rows[1][2],
                    ))

    def __hash__(self) -> int:
        return hash(tuple(self))


class Rot(Mat):
    def __init__(self, angle):
        super(Rot, self).__init__(angle, (0, 0), (1, 1))


class Tra(Mat):
    def __init__(self, x, y):
        super(Tra, self).__init__(0, (x, y), (1, 1))


class Sca(Mat):
    def __init__(self, x, y):
        super(Sca, self).__init__(0, (0, 0), (x, y))


def polar(v):
    r = ()
    for x, y in pairwise(v):
        r += (sqrt(x ** 2 + y ** 2), degrees(atan2(y, x)))
    return r


def cartesian(v):
    r = ()
    for rho, theta in pairwise(v):
        theta = radians(theta)
        r += (rho * cos(theta), rho * sin(theta))
    return r


def pairwise(it):
    m = iter(it)
    while True:
        try:
            yield (next(m), next(m))
        except StopIteration:
            return
    return


class TransformInterface:
    
    def __matmul__(self, other):
        """transform"""
        return self._incremental[-1] @ other
    
    def zero(self):
        """
        The zero of the current position in world coordinates
        :return: 
        """
        return self._incremental[-1].translation
    
    def __getitem__(self, position):
        return self._incremental[position]


def _take(how_many):
    def fu(li):
        if how_many < 0:
            raise IndexError
        if how_many == 1:
            li.pop()
        else:
            del li[-how_many:]
    return fu


class TransformStack(TransformInterface):
    """
    could be considered a matrix itself
    """

    def __init__(self):
        self._incremental = []
        self._transforms = []
        self._saved_positions = OrderedDict()
        self.clear()
    
    def __copy__(self):
        ts = TransformStack()
        ts._incremental[:] = self._incremental
        ts._transforms[:] = self._transforms
        for x, v in self._saved_positions.items():
            vc = TransformProxy(ts, x)
            vc._incremental = v._incremental[:]
            ts._saved_positions[x] = vc
        return ts
    
    def _both(self, func, *args):
        for x in self._incremental, self._transforms:
            func(x, *args)
    
    def __len__(self):
        return len(self._incremental)
    
    def world(self):
        return self
    
    def clear(self):
        self._both(list.clear)
        self._saved_positions.clear()
        mid = Mat.identity()
        self._both(list.append, mid)
        
    def pop_multiple(self, how_many=1, maintain_saved=False):
        next_one = None
        m = self._transforms[-1]
        self._both(_take(how_many))
        if not self._incremental:
            m = Mat.identity()
            self._both(list.append, m)

        to_pop = set()
        next_one = self
        for key, proxy in self._saved_positions.items():
            len_current = len(proxy)
            if len_current > how_many:
                proxy._pop(how_many)
                if len_current < len(self):
                    next_current = proxy
            else:
                proxy.invalid()
                to_pop.add(key)
                
        if not maintain_saved:
            for x in to_pop:
                del self._saved_positions[x]
        return next_one, m

    def pop(self, how_many=1):
        return self.pop_multiple(how_many, maintain_saved=True)[1]
    
    
    def __incrementals(self):
        return itertools.chain([self._incremental],
                        self._saved_positions.values())

    def __ixor__(self, other):
        """replace the tip of the stack with other"""
        self.pop()
        self >> other
        return self

    def __irshift__(self, other):
        """replace the tip of the stack, multiplying it right with other"""
        
        self._incremental[-1] = self._incremental[-1] @ other
        for incs in self._saved_positions.values():
            incs._rightmerge(other)
        self._transforms[-1] = self._transforms[-1] @ other
        return self
    
    def __ilshift__(self, other):
        """replace the tip of the stack, multiplying it left with other"""
        self._transforms[-1] = other @ self._transforms[-1]
        self._incremental[-1] = self._incremental[-2] @ self._transforms[-1]
        for incs in self._saved_positions.values():
            incs._leftmerge(self._transforms[-1])
        
        return self
    
    
    def __rshift__(self, other):
        self._incremental.append(self._incremental[-1] @ other)
        for incs in self._saved_positions.values():
            incs._rightaccrue(other)
        self._transforms.append(other)
    
    def remember(self, ob):
        """ Saves the position (creating a new proxy)"""
        self >> Mat.identity()
        try:
            proxy = self._saved_positions[ob]
            proxy.stack = self
            proxy.clear()
        except KeyError:
            proxy = TransformProxy(self, ob)
            self._saved_positions[ob] = proxy
        return proxy
    
    def proxy(self, ob):
        return self._saved_positions[ob]
    
            
    def base(self, ob=None):
        """
        NOTE: this returns something that tells you the world coordinates from the
        local coordinates (= base); that is, the position where the context
        ob was remembered
        :param ob: 
        :return: 
        """
        if ob:
            return self._incremental[-len(self._saved_positions[ob])]
        else:
            return self[0]
    
    def forget(self, ob):
        self.pop_multiple(len(self._saved_positions[ob])) 
        return self
    
    def reset(self, ob):
        self.pop_multiple(len(self._saved_positions[ob])-1)
        return self
    
        
    def __str__(self):
        return f"[T:{str(self._incremental[-1])}]"

    def __hash__(self):
        return hash(tuple(self._transforms))
    
    def shrink(self):
        t = self.pop()
        self >>= t
        
class TransformProxy(TransformInterface):
    """
    This object modifies an original stack, and keeps a list of incremental
    transforms. Any change is reflected to the original stack.
    The purpose is to consider only a partial transform while still modifying the
    transform stack.
    """
    def world(self):
        return self.stack
    
    def __init__(self, transform_stack, identifier):
        self.stack = transform_stack
        self.identifier = identifier
        self._incremental = []
        self.clear()
    
    def clear(self):
        self._incremental = [self.stack._transforms[-1]]
     
    def __len__(self):
        return len(self._incremental)
    
    def __rshift__(self, other):
        """Right multiplication, accrue"""
        self.stack >> other
        return self
    
    def __irshift__(self, other):
        """Right multiplication, in place"""
        self.stack >>= other
        return self
    
    def __ilshift__(self, other):
        """left multiplication, in place"""
        self.stack <<= other
        return self
        
    def __ixor__(self, other):
        """Substitution, in place"""
        self.stack ^= other
        return self
        
    def pop(self):
        self.stack.pop()
    
    def base(self):
        return self.stack[-len(self)]
        
    def _pop(self, how_many=1):
        _take(how_many)(self._incremental)
        
    def _rightaccrue(self, other):
        incs = self._incremental
        try:
            incs.append(incs[-1] @ other)
        except IndexError:
            incs.append(other)
    
    def _rightmerge(self, other):
        self._incremental[-1] = self._incremental[-1] @ other
    
    def _leftmerge(self, other):
        try:
            mtx = self._incremental[-2] @ other
        except:
            mtx = other
        self._incremental[-1] = mtx

    def proxy(self, ob):
        return self.stack.proxy(ob)
    
    def remember(self, ob):
        return self.stack.remember(ob)
    
    def forget(self, ob=None):
        """Removes this proxy, and returns the last proxy entered"""
        next_one, _ = self.stack.pop_multiple(len(self))
        return next_one
    
    def reset(self, ob):
        self.stack.pop_multiple(len(self)-1)
        return self
    
    def invalid(self):
        self.stack = None
        
    def __copy__(self):
        sc = copy(self.stack)
        return sc.proxy(self.identifier)
        
if __name__ == "__main__":
    ts = TransformStack()
    ts >> Mat.identity()
    ts >> Tra(10,10)
    ctx = ts.remember("Caro edu")
    
    print(ts @ (0,0))
    print(ctx @ (0, 0))
    ctx >>= Tra(10, 0)
    print(ts @ (0, 0))
    print(ctx @ (0, 0))
    ts.forget("Caro edu")