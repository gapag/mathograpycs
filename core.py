from collections import OrderedDict
from copy import copy, deepcopy

from vysitors.vysitors import Visitor

from algebra import TransformStack, cartesian, Mat



class Properties:
    def __init__(self, **kwargs):
        self.kwargs = kwargs

    def __enter__(self):
        pass

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass


class Drawable:
    pass


class DrawTuple(tuple):
    def __new__(self, tup=()):
        tt = super(DrawTuple, self).__new__(DrawTuple, tup)
        tt.close = False
        return tt

    def __hash__(self):
        m = type(self), super(DrawTuple, self).__hash__(), self.close
        return hash(m)


class MoveTuple(tuple):
    
    def __new__(self, tup):
        return super(MoveTuple, self).__new__(MoveTuple, tup)

    def __hash__(self):
        m = type(self), super(MoveTuple, self).__hash__()
        return hash(m)


class SaveLabel(str):

    def __new__(cls, s):
        return super(SaveLabel, cls).__new__(SaveLabel, s)


class RestoreLabel:

    def __init__(self, label):
        self.label = label
    

class DrawToLabel(str):
    def __new__(cls, s):
        return super(DrawToLabel, cls).__new__(DrawToLabel, s)

class MoveToLabel(str):
    def __new__(cls, s):
        return super(MoveToLabel, cls).__new__(MoveToLabel, s)


class Sequence:
    def __init__(self, item=None):
        self.points = [item] if item is not None else []

    def __append(self, other, tuptyp, restoretyp, closetyp):

        if isinstance(other, Sequence):
            self.points = self.points + other.points
        else:
            if isinstance(other, tuple):
                other = tuptyp(other)
            elif isinstance(other, str):
                other = SaveLabel(other)
            elif isinstance(other, RestoreLabel):
                other = restoretyp(other.label)
            elif isinstance(other, Close):
                other = closetyp()
            
            self.points.append(other)
        return self

    def __rshift__(self, other):
        self.__append(other, DrawTuple, DrawToLabel, DrawClose)
        return self

    def __lshift__(self, other):
        self.__append(other, MoveTuple, MoveToLabel, MoveClose)
        return self

    def __iter__(self):
        return iter(self.points)
    
    def __hash__(self):
        return hash(tuple(self.points))

class NewReference: 
    def __init__(self, pen=None):
        self.pen = pen
    
    def __str__(self):
        return f"nr<{self.pen}>"
        

class PenMode:
    def __init__(self, relative=False, polar=False, close=False):
        # super(PenMovement, self).__init__(*args)
        self.relative = relative
        self.polar = polar
        
    def __str__(self):
        return f"{'r' if self.relative else ''}{type(self)}" \
               f"{'p' if self.polar else ''}"

    def __rshift__(self, other):
        """
        :param other: 
        :return: 
        """
        p = Sequence(self) >> other
        return p

    def __lshift__(self, other):
        p = Sequence(self) << other
        return p

    def __eq__(self, other):
        return (type(self), self.polar, self.relative) == \
               (type(other), other.polar, other.relative)

    def __hash__(self):
        return hash((type(self), self.relative, self.polar))


class Reset:
    pass


class Close:
    pass

class DrawClose:
    pass

class MoveClose:
    pass




class AttributeNotFound(AttributeError): pass


class Pen:
    def __init__(self, name='',
                 defaults=None,
                 **kwargs):
        self.name = name
        
        self.properties = kwargs
        self.defaults = defaults or {}

    def __getattr__(self, item):
        try:
            return super(Pen, self).__getattr__(item)
        except:
            try:
                return self.properties[item]
            except KeyError:
                raise AttributeNotFound(item)

    def __getitem__(self, item):
        return self.properties.get(item, self.defaults.get(item, None))

    def __str__(self):
        return f"pen:{self.name}"

    
    def __rshift__(self, other):
        """
        :param other: 
        :return: 
        """
        p = Sequence(self) >> other
        return p
    
    def __lshift__(self, other):
        p = Sequence(self) << other
        return p
    
    def __hash__(self):
        return hash(self.name)
    
    def __call__(self, *args, **kwargs):
        return PenChange(self)
    

class IncompatibleSuccessor(TypeError):
    pass


class PolarCurve(Drawable):
    def __init__(self, radius_fun, init_radius=0,
                 init_angle=0,
                 divergence_fun=lambda r, a: a + 1,
                 of=None,
                 iterations=600, label="of"):
        super(PolarCurve, self).__init__()
        self.radius = radius_fun
        self.divergence = divergence_fun
        self.init_radius = init_radius
        self.init_angle = init_angle
        self.of = of
        self.iterations = iterations
        self.pen = None

    def __call__(self, label="of"):

        a = self.init_angle
        r = self.init_radius
        
        points = Sequence()
        points << (0,0)
        tu = ()
        for s in range(1, self.iterations + 1):
            
            r = self.radius(r, a)
            a = self.divergence(r, a)
            
            if self.of:
                points << (r, a)
                try:
                    of = self.of(r, a)
                except TypeError:
                    of = self.of
                points << label >> of << restore(label)
                
            else:
                tu += (r, a)
        if not self.of:
            points >> tu
        return points


class Polygon(Drawable):

    def __init__(self, r, v, angle=0, star=1):
        self.r = r
        self.v = v
        self.a = angle
        self.star = star

    def __call__(self):
        """
        * make a dictionary D of the vertices
        * while the dictionary is not empty:
        *   pop one entry a 
        *   move the pen there
        *   while there is not a miss:
        *       pop the one at b = a+li.star mod li.v
        *       draw a line a->b
        *       a = b
        :param li: 
        :return: 
        """

        _d_ = OrderedDict({t: 360 * t / (self.v)
                           for t in range(0, self.v)})

        cc = Sequence()
        while len(_d_):
            "take a vertex and an angle"
            k, aa = next(iter(_d_.items()))
            "remove that vertex"
            _d_.pop(k)
            "move to that place"
            tu = ()
            cc = cc << (self.r, aa)
            
            while len(_d_):
                d, k = divmod(k + self.star, self.v)
                try:
                    aa = _d_.pop(k)
                    tu += (self.r, aa)
                    
                except KeyError:
                    break
            cc = cc >> tu >> close()
        
        return cc


class Spiral(PolarCurve):
    def __init__(self, **kwargs):
        super(Spiral, self).__init__(**kwargs)

    @classmethod
    def daisy(cls, r, a):
        return a + 360 / 1.618034


class Canvas:
            
    def __init__(self, width=0, height=0, emitter=None):
        self.height = height
        self.width = width
        self.items = set()
        self.commands = []
        self.emitter = emitter

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type is None:
            self.draw()
        

    def __contains__(self, item):
        """overridden semantics. This function does not check for 
        membership but adds an element."""
        if not isinstance(item, Sequence):
            item = Sequence(item)
        self._add_command(item)

    def _add_command(self, item):
        self.define(item)
        self.commands.append(item)

    def define(self, item):
        self.items.add(item)

    def draw(self):
        if self.emitter is not None:
            self.emitter.emit(self)
        try:
            self.emitter.save()
        except AttributeError:
            pass

    def pen(self, *args, **kwargs):
        return self.emitter.arm.new_pen(*args, **kwargs)
    
    def __iter__(self):
        return iter(self.commands)

    def transform(self, angle=0, x=0, y=0, sx=0, sy=0):
        try:
            yield 9
        finally:
            pass


def cart():
    return PenMode(relative=False, polar=False)


def pol():
    return PenMode(relative=False, polar=True)

def newref():
    return NewReference()


def rcart():
    return PenMode(relative=True, polar=False)


def rpol():
    return PenMode(relative=True, polar=True)


def reset():
    return Reset()


def close():
    return Close()

def restore(lab):
    return RestoreLabel(lab)

def transform(**kwargs):
    return Mat(**kwargs)


def path(values_as_pairs):
    for x, y in values_as_pairs:
        yield x
        yield y
    raise StopIteration


class Context:
    def __init__(self, pen, mode, arm):
        self.current_path = []
        self.pen = pen
        self.mode = mode
        self.arm = arm
        
        
    def set_pen(self, pen):
        self.pen = pen
        
    
    def restore(self, reference=None):
        self.arm.position().forget(reference)
        
    
    def set_mode(self, mode):
        self.mode = mode
        
    def clone(self):
        return type(self)(self.pen, self.mode, self.arm)

    

    
        

class Arm:

    def __init__(self, context_cls=None, pen_cls=None):
        self._pens = set()
        
        self._translation_attributes = {}
        ctx = context_cls or Context
        self.pen_cls = pen_cls or Pen
        self.down_at = None
        self._position: TransformStack = TransformStack()
        self._position >> Mat.identity()
        self.pen_has_changed = False
        self._context = ctx(self.new_pen(), cart(), self)
        self._labeled_stacks = OrderedDict()
        self.reset()
        
        
        
    def pen_changed(self):
        return self.pen_has_changed and self.down_at is not None
        
    def pen_down(self):
        self.down_at = self._position.zero()
        
    def pen_up(self):
        self.down_at = None
        self.pen_has_changed = False
    
    def _attributes_(self, *_classes):
        ats = {}
        for cl in _classes:
            ats.update({x : self.pen[x] 
                        for x in self._translation_attributes[cl]
                        if self.pen[x] is not None})
        return ats
    
    def reset(self):
        self._position = TransformStack()
        self._position >> Mat.identity()
        self.pen_up()
        self.clear_context()
        self._labeled_stacks.clear()
    
    def open_path(self):
        if self.down_at is None:
            self.pen_down()
    
    
    def _pen_changed_close(self, tupletype, emitter):
        oldmode = self.mode
        self.mode = cart()
        emitter.tuple(tupletype(self.down_at))
        self.mode = oldmode
    
    def _same_pen_close(self):
        old = self.remove_last_chunk()
        self.add_chunk(old + ('z',))
    
    def _close(self, tupletype, emitter):
        if self.pen_changed():
            self._pen_changed_close(tupletype, emitter)
        else:
            self._same_pen_close()
        self.pen_up()
        
    def draw_close(self, emitter):
        self._close(DrawTuple, emitter)
    
    def remove_last_chunk(self):
        raise NotImplemented()
    
    def add_chunk(self, partial):
        raise NotImplemented()
    
    def close_path(self):
        tu = None
        if self.down_at:
            tu = DrawTuple(self.down_at)
        self.pen_up()
        return tu
        
    def getpen(self):
        return self._context.pen

    def setpen(self, value):
        self._context.set_pen(value)
        if self.down_at is not None:
            self.pen_has_changed = True
        
    def getmode(self):
        return self._context.mode
    
    def setmode(self, value):
        self._context.set_mode(value)
    
    def getcontext(self):
        return self._context
        
    pen = property(getpen, setpen, doc="pen in the current context")
    mode = property(getmode, setmode, doc="mode in the current context")
    context = property(getcontext, doc="the current context")
    
    def clear(self):
        self.down_at = None
        self.stackstack.clear()
        self.reset()
    
    def clear_context(self):
        self._context = type(self._context)(self.new_pen(), cart(), self)
    
    def new_pen(self, defaults=None, **kwargs):
        p = self.pen_cls(defaults=defaults, **kwargs)
        self._pens.add(p)
        
        return p
    
    def position(self, at=None):
        if at is not None:
            return self._labeled_positions[at]
        return self._position
    
    def save(self, key):
        cp = copy(self._position)
        self._labeled_stacks[key] = cp
    
    def new_reference(self, item, with_name=None):
        if with_name is not None:
            key = with_name
        else:
            key = item
        return self._position.remember(key)
    
    def restore(self, key):
        self._position = self._labeled_stacks[key]
        
        


    def move_relative(self, x, y, polar):
        if polar:
            # Get the current world coordinate of the pen.
            # This is needed because we are returning the cartesian relative 
            # displacement coordinates.
            xw, yw = self._position.zero()
            # Get the absolute cartesian coordinates of the cartesian relative
            # version of the relative polar coordinates x,y
            _cx, _cy = cartesian((x, y))
            _xw, _yw = self._position @ (_cx, _cy)
            # Rotate by y, advance by x
            m = Mat(y) @ Mat(translation=(x, 0))
            _nx, _ny = _xw - xw, _yw - yw
            self._position >>= m
        else:

            # Translate by the relative segment x,y
            m = Mat(0, (x, y))
            _nx, _ny = x, y
            self._position <<= m
        # Advance by the relative displacement given by m

        return _nx, _ny

    def move_absolute(self, x, y, polar):
        
        # Absolute.
        if polar:
            # Orient with y, then move ahead of x
            m = Mat(y) @ Mat(translation=(x, 0))
        else:
            # Translate of x and y
            m = Mat(0, (x, y))
        # Since it is absolute, replace and don't combine
        # the tip of the transform stack.
        self._position ^= m
        v = self._position.zero()
        
        return v 

    def move(self, x, y, mode):
        """
        Update the pen by moving it by the pair of coordinates x and y.
        Their interpretation depends on the current mode of the pen. 
        :return: 
            The displacement from the previous position, in cartesian coords.

        """
        polar = mode.polar
        relative = mode.relative

        if relative:
            mvm = self.move_relative(x, y, polar)
        else:
            mvm = self.move_absolute(x, y, polar)

        return mvm

class Emitter(Visitor):

    def __init__(self, filename, *args, debug=False, pretty=False, context_cls=None,  **kwargs):
        super(Emitter, self).__init__(*args, **kwargs)
        self.pretty = pretty
        self.debug = debug
        self.arm = Arm(context_cls=context_cls)
        self.filename = filename
    
    def emit(self, thing):
        return self.visit(thing)
    
    def Pen(self, item, *args, **kwargs):
        self.arm.pen = item
        
    def PenMode(self, item, *args, **kwargs):
        self.arm.set_movement(item)
    
    def Reset(self, *args, **kwargs):
        self.arm.pen.position.reset(self.arm.mode)
        
    def NewReference(self, item, *args, **kwargs):
        self.arm.new_reference(item)
        
    def SaveLabel(self, item, *args, **kwargs):
        self.arm.new_reference(item)
        # self.arm.save(item)
        
    def RestoreLabel(self, item, *args, **kwargs):
        self.arm.restore(item)