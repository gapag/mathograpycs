from core import Canvas, cart, rcart, close, pol, rpol
from emit.continuous import ContinuousPath
"""
Move the pen using polar coordinates.
"""
with Canvas(600,300, emitter=ContinuousPath(f'{__file__}.svg',debug=True)) as canvas:
    black = canvas.pen(name="cross", stroke="black", stroke_width=1)
    blue = canvas.pen(name="first", stroke="blue", stroke_width=3)
    green = canvas.pen(name="small", stroke="green", stroke_width=3)

    s0 = blue >> pol() >> (100, 45, 100, 90, 100, 100)
    s1 = green >> rpol() >> (20, 20, 30, 20, 40, 20, 50,20)
    abscissa = cart() >> black << (0, 0) >> (0, 0, 100, 0) >> rcart() >> (-10, -10)
    ordinata = cart() >> black << (0, 0) >> (0, 0, 0, 100) >> rcart() >> (-10, -10)

    s0 in canvas
    s1 in canvas
    abscissa in canvas
    ordinata in canvas
