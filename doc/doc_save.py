from core import Canvas, cart, rcart, close, Polygon, pol, restore, newref
from emit.continuous import ContinuousPath
"""
Save position.
"""
with Canvas(600,300, emitter=ContinuousPath(f'{__file__}.svg', debug=True) ) as canvas:
    black = canvas.pen(name="cross", stroke="black", stroke_width=1)
    blue = canvas.pen(name="first", stroke="blue", stroke_width=3)
    green = canvas.pen(name="small", stroke="green", stroke_width=3)

    poly = Polygon(30,7,star=1)

    abscissa = cart() >> black << (0, 0) >> (0, 0, 100, 0) >> rcart() >> (-10, -10)
    ordinata = cart() >> black << (0, 0) >> (0, 0, 0, 100) >> rcart() >> (-10, -10)

    def shape(pen, center):
       return pen << center >> newref() >> poly >> restore("origin") >> "origin"

    complicated = pol() << (60,60) >> newref() >> "origin" >> \
                  shape(black, (70, -90)) >> \
                  shape(blue, (70,0)) >> \
                  shape(green, (70, 90))

    abscissa in canvas
    ordinata in canvas
    complicated in canvas
