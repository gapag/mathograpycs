from core import Canvas, cart, rcart, close
from emit.continuous import ContinuousPath
"""
Move the pen using cartesian coordinates.
"""
with Canvas(600,300, emitter=ContinuousPath(f'{__file__}.svg')) as canvas:
    black = canvas.pen(name="cross", stroke="black", stroke_width=1)
    blue = canvas.pen(name="first", stroke="blue", stroke_width=3)
    green = canvas.pen(name="small", stroke="green", stroke_width=3)

    s0 = blue << (100, 100) >> (100, -100, -100, -100, -100, 100)

    abscissa = cart() >> black << (0, 0) >> (0, 0, 100, 0) >> rcart() >> (-10, -10)
    ordinata = cart() >> black << (0, 0) >> (0, 0, 0, 100) >> rcart() >> (-10, -10)

    def square(x):
        return x >> (50,0, 0, 50, -50, 0) >> close() << (100, -100)
    s0 in canvas
    abscissa in canvas
    ordinata in canvas
    square(green) in canvas
    square(blue) in canvas
