import math

from core import Canvas, cart, rcart, close, pol, rpol, newref, Spiral, Polygon
from emit.continuous import ContinuousPath
"""
Some shapes.
"""
with Canvas(600,300, emitter=ContinuousPath(f'{__file__}.svg',debug=False)) as canvas:
    black = canvas.pen(name="cross", stroke="black", stroke_width=1)
    blue = canvas.pen(name="first", stroke="blue", stroke_width=3)
    green = canvas.pen(name="small", stroke="green", stroke_width=3)
    its = 50
    poli = Polygon(10, 5, star=1)
    spi = Spiral(radius_fun=lambda r, a: .95 * math.sqrt(a),
             #divergence_fun=lambda r, a: a+10,
             divergence_fun=Spiral.daisy,
             of=poli,
             iterations=its,
             )

    spi_line = Spiral(radius_fun=lambda r, a: 5 * math.sqrt(a),
                  divergence_fun=lambda r, a: a + 10,
                  iterations=its,
                  )

    spiral = green >> pol() >> spi_line
    daisy = blue << pol() << (150, 15) >> newref() >> spi

    abscissa = cart() >> black << (0, 0) >> (0, 0, 100, 0) >> rcart() >> (-10, -10)
    ordinata = cart() >> black << (0, 0) >> (0, 0, 0, 100) >> rcart() >> (-10, -10)

    spiral in canvas
    abscissa in canvas
    ordinata in canvas
    daisy in canvas
