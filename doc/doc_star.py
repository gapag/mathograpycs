import math

from core import Canvas, cart, rcart, close, pol, rpol, newref, Spiral, Polygon
from emit.continuous import ContinuousPath
"""
Polygons and stars.
"""
with Canvas(600,300, emitter=ContinuousPath(f'{__file__}.svg',debug=False)) as canvas:
    black = canvas.pen(name="cross", stroke="black", stroke_width=1)
    blue = canvas.pen(name="first", stroke="blue", stroke_width=3)
    green = canvas.pen(name="small", stroke="green", stroke_width=3)
    p = pol()
    abscissa = cart() >> black << (0, 0) >> (0, 0, 100, 0) >> rcart() >> (-10, -10)
    ordinata = cart() >> black << (0, 0) >> (0, 0, 0, 100) >> rcart() >> (-10, -10)
    for c, x in zip([green, blue, black], range(1,4)):
        p = p >> c >> Polygon(20, 7, star=x) << cart() << (50,0) >> newref() >> pol()
    abscissa in canvas
    ordinata in canvas
    p in canvas

