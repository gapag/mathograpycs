from core import Canvas, cart, rcart, close, pol, rpol, newref
from emit.continuous import ContinuousPath
"""
Switch coordinate system.
"""
with Canvas(600,300, emitter=ContinuousPath(f'{__file__}.svg',debug=True)) as canvas:

    black = canvas.pen(name="cross", stroke="black", stroke_width=1)
    blue = canvas.pen(name="first", stroke="blue", stroke_width=3)
    green = canvas.pen(name="small", stroke="green", stroke_width=3)

    abscissa = cart() >> black << (0, 0) >> (0, 0, 100, 0) >> rcart() >> (-10, -10)
    ordinata = cart() >> black << (0, 0) >> (0, 0, 0, 100) >> rcart() >> (-10, -10)
    move = cart() << (0, 0) << pol() << (100,30) >> newref()
    square =  blue >> cart() >> (50,0,50,50,0,50,0,0)

    abscissa in canvas
    ordinata in canvas
    move in canvas
    square in canvas