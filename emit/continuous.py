from itertools import chain

import svgwrite as svgwrite
from svgwrite.container import Group, Use
from svgwrite.path import Path
from vysitors.vysitors import Annotations as _a_

from algebra import pairwise, cartesian, Mat
from core import PenMode, Pen, Arm, newref, cart, DrawTuple, MoveTuple
from emit.define import Define
from emit.svg import SVGBaseEmitter, move_only_path, _PenUpdate
from stackstack import StackStack


class NoDefinitionsEmitter(SVGBaseEmitter):

    
        

    @_a_.children(children_first=False)
    def Canvas(self, cv, *args, **kwargs):
        super(NoDefinitionsEmitter, self).Canvas(cv, *args, **kwargs)
        self.svg.embed_stylesheet(self.arm.make_style())
        
    
        
    def NewReference(self, item, *args, **kwargs):
        super(NoDefinitionsEmitter, self).NewReference(item, *args, **kwargs)
        self.arm.pen_up()
        self.start_path()

    def SaveLabel(self, item, *args, **kwargs):
        self.arm.save(item)
        self.NewReference(item,*args, **kwargs)
        self.start_path()

    def MoveToLabel(self, item, *args, **kwargs):
        self.arm.restore(item)
        self.start_path()
        
    def DrawToLabel(self, item, *args, **kwargs):
        # TODO
        self.arm.restore(item)
    
    def DrawClose(self, item, *args, **kwargs):
        self.arm.draw_close(self)
    
    def MoveClose(self, item, *args, **kwargs):
        if self.arm.down_at is not None:
            self.__close(MoveTuple)
        
    def Pen(self, item, *args, **kwargs):
        # Create a new path from the current 
        # Add it to svg
        if self.arm.pen is item:
            return
        self.finish_path()
        super(NoDefinitionsEmitter, self).Pen(item, *args, **kwargs)
        self.start_path()
        
    def MoveTuple(self, item, *args, **kwargs):
        self.arm.pen_up()
        self.tuple(item, *args, **kwargs)
    
    def tuple(self, item, *args, **kwargs):
        
        partial = ()
        for (x, y), cmd in zip(pairwise(item), self._generator(item)):
            _x, _y = self.arm.move(x, y, self.arm.mode)
            partial += ((cmd,) if cmd else ()) + (_x, _y) 
        
        self.arm.add_chunk(partial)
        self.add_axes(self.arm.position().world()[-1])
        return item
    
    def Drawable(self, item, *args, **kwargs):
        self.finish_path() # clear the previous accrued path
        exp = item() # expand the drawable
        self.start_path()
        self.emit(exp) # Does not have to invoke finish_path(): Sequence will do  
        self.start_path()
        
    def finish_path(self):
        p = self.arm.finish_path()
        if p:
            self.svg.add(p)
            
    def start_path(self):
        self.arm.open_path()

    @_a_.children(children_first=True)
    def Sequence(self, pa, *args, **kwargs):
        self.finish_path()
        self.start_path()
        return kwargs['children_result']

class ContinuousArm(Arm):
    
    def __init__(self):
        super(ContinuousArm, self).__init__()
        self.stackstack = StackStack([0,1])
        self._translation_attributes = _PenUpdate.svg_args
        
        
     
    
    def make_style(self):
        s=""
        for x in filter(lambda y:y.name,self._pens):
            s += f".{x.name}{{"
            for att in self._translation_attributes[Path]:
                if x[att]:
                    s+=f"{att.replace('_','-')}:{x[att]};"
            s += "}\n"
        return s
        
    
    def move_relative(self, x, y, polar):
        """
        Cartesian relative are with respect to the current reference system.
        Polar relative instead are relative to the current position.
        :param x: 
        :param y: 
        :param polar: 
        :return: 
        """
        
        if polar:
            # Get the cartesian value of the polar relative  
            xw, yw = self._position.zero()
            _cx, _cy = cartesian((x, y))
            # Rotate by y, advance by x
            m = Mat(y) @ Mat(translation=(x, 0))
            # Advance by the relative displacement given by m
            _xw, _yw = self._position @ (_cx, _cy)
            _nx, _ny = _xw - xw, _yw - yw
            self._position >>= m
        else:
            # Get the absolute cartesian coordinates of the cartesian relative
            # version of the current reference system
            xw, yw = self._position[-2].zero()
            _cx, _cy = x, y
            # Translate by the relative segment x,y
            m = Mat(0, (x, y))
            # Advance by the relative displacement given by m
            _xw, _yw = self._position[-2] @ (_cx, _cy)
            _nx, _ny = _xw - xw, _yw - yw
            self._position <<= m
        
        return _nx, _ny
    
    def add_chunk(self, partial):
        self.stackstack.push(partial)
    
    def remove_last_chunk(self):
        return self.stackstack[0].pop()
    
    
    
    def open_path(self):
        super(ContinuousArm, self).open_path()
        self.stackstack.push(('M',) + self.position() @ (0, 0))
        
    def __remove_repetitions_of_l_or_L(self, cp):
        last_seen, d = None, ()
        for x in cp:
            if x[0] == last_seen and x[0].upper() != "M":
                x = x[1:]
            last_seen = x[0]
            d += x
        return d
    
    def finish_path(self):
        cp = self.stackstack[0]
        p = None
        if cp:
            d = self.__remove_repetitions_of_l_or_L(cp)
            if not move_only_path(d):
                p = Path(d=d, class_=self.pen.name)
        self.stackstack.clear()
        return p

    


class ContinuousPath(NoDefinitionsEmitter):
    def __init__(self, *args, **kwargs):
        super(ContinuousPath, self).__init__(*args, **kwargs)
        self.arm = ContinuousArm()
    