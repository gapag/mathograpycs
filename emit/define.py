from collections import Counter
from copy import copy
from itertools import chain

import svgwrite as svgwrite
from svgwrite import Drawing
from svgwrite.container import Group, Use, Style, Defs
from svgwrite.path import Path

from vysitors.vysitors import Annotations as _a_, Visitor
from algebra import pairwise, TransformStack, Tra
from core import Canvas, Mat, Polygon, \
    cart, Spiral, cartesian, PolarCurve, \
    AttributeNotFound, pol, rpol, Emitter, Reset, \
    Sequence as MPath, PenMode, DrawTuple, MoveTuple, Pen, Arm, Context, \
    Sequence, NewReference, Close, newref
from emit.svg import SVGBaseEmitter, move_only_path, _PenUpdate
from stackstack import StackStack


class MissingCommand(Exception):
    pass

            

class SVGDefsContext(Context):
    
    def __init__(self, *args):
        super(SVGDefsContext, self).__init__(*args)
#        self.repository = DefinitionRepository()
        self.position = self.arm.position()
        
    
    def set_pen(self, pen):
        super(SVGDefsContext, self).set_pen(pen)
        
        
    def set_mode(self, mode):
        super(SVGDefsContext, self).set_mode(mode)
    
    def clone(self):
        cl = super(SVGDefsContext, self).clone()
        cl.repository = self.repository
        return cl


class ProtectedRepository(Exception):
    pass


class DefinitionRepository:
    def __init__(self, pen):
        self.cnt = Counter()
        self.definitions = {}
        self.usages = {}
        self.references = [(None, pen)]
        self.styles = {}
        self.protect = False
        
        """
        Definitions' keys (or):
        - The d= parameter of a path for a path.
        - A Drawable object
        Definitions' values:
        - SVG objects <g> or <path>
        Usages' keys:
        - Mode and _tuple_
        - Mode, (x0, y0), _tuple_ (to address intermediate paths)
        - Drawable 
        Usages' values:
        - SVG item being used (<g> or <path>), differential trasform it determines 
        """

    def __contains__(self, item):
        return item in self.definitions
    
    def create_css_pen_rule(self, uid, _pen):
        s = f"#{uid}{{"
        for x in _PenUpdate.svg_args[Path]:
            s += f"{x.replace('_','-')} : var(--{x}-{uid});"
        return s+"}"
    
    def simplify(self, key):
        if len(key) == 1:
            k = self.simplify(key[0])
            if k is ():
                return ()
        return key
        
            
    def defined(self, key):
        return self.simplify(key) in self.definitions
    
    def store(self, key, value):
        if self.protect:
            raise ProtectedRepository(key, value)
        k = self.simplify(key)
        if k is ():
            value['id'] = "Void"
        self.definitions[k] = value
    
    def retrieve(self, key):
        return self.definitions[self.simplify(key)]
    
    def make_group(self, ty, idx, val, position, instance):
        
        if self.defined(val):
            return
        g = Group(id=f"{ty.__name__}_{self.cnt[ty]}")

        using_pen = ty is Sequence
        for e, x in enumerate(val):
            if x:
                chidef = self.retrieve(x)
                ref = None
                uid = None
                
                if using_pen:
                    ref, _pen = self.references[e]
                    uid = "pen"
                    
                child = self.use(chidef, ref, usage_id=uid)
                void = child.href == "#Void"
                if uid and not void:
                    chid = child['id']
                    self.styles[chid] = self.create_css_pen_rule(chid, _pen)
                
                g.add(child) 
        
        self.store(val,g)
        self.cnt[ty]+=1
        
    
    def use(self, path, base_transform=None, usage_id=None):
        refname = str(path.attribs['id'])
        uid = None
        if usage_id:
            uid = f"{usage_id}_{self.cnt[usage_id]}"
            self.cnt[usage_id] += 1
        u = Use("#" + refname, id=uid)
        if base_transform:
            u.matrix(*list(base_transform))
        return u
        
    def store_path(self, d):
        if not self.defined(d):
            p = Path(d, id=self.cnt[tuple])
            self.cnt[tuple] += 1
            self.store(d,p)
        else:
            p = self.retrieve(d)
        return p
    
    def use_group(self, key, base=None, usage_id=None):
        de = self.retrieve(key)
        return de, self.use(de, base, usage_id)
    
    def add_pen(self, pen):
        pass
    
    


class DefiningArm(Arm):
    def __init__(self):
        super(DefiningArm, self).__init__(SVGDefsContext)
        self.repository = DefinitionRepository(self.pen)
        self.stackstack = StackStack([PenMode, Close, NewReference, Sequence, Canvas], 
                                     hook=self.hook)
        self._translation_attributes = _PenUpdate.svg_args
        self.styles = {}
        
    def instantiating(self):
        self.repository.protect = True
    
    def translate_styles(self):
        s = ""
        for di in [self.repository.styles, self.styles]:
            for x in di.values():
                s+=x+"\n"    
        return s
    
    def hook(self, ty, idx, val, o, instance):
        self.repository.make_group(ty, idx, val, self._position, instance)
    
        
    def setmode(self, mode):
        self.group(mode)
        super(DefiningArm, self).setmode(mode)
        
    def add_chunk(self, partial):
        if not move_only_path(partial):
            self.stackstack.push(partial)
            self.repository.store_path(partial)
    
    def group(self, item, base=None, usage_id=None):
        val = self.stackstack.level(type(item), item)
        return self.repository.use_group(val, base, usage_id)
        
    def setpen(self, pen):
        #self.group(pen)
        b = self.group(NewReference(pen), self._position.base())
        self.repository.references.append((self._position.base(), pen))
        super(DefiningArm, self).setpen(pen)
    
    def new_reference(self, item, with_name=None):
        b = self.group(item, self._position.base())
        
        self._position = super(DefiningArm, self).new_reference(item, with_name)
        
        self.repository.references.append((self._position.base(), self.pen))
        self.pen_up()
        return b
    
    def restore(self, item=None):
        b = self.group(newref(), self._position.base())
        
        super(DefiningArm, self).restore(item)
        
        self.repository.references.append((self._position.base(), self.pen))
        self.pen_down()
        
    
    def reset_reference_list(self):
        oldpos = self.repository.references[-1]
        self.repository.references = [(None, self.repository.references[-1][1])]
        return oldpos

    
        
    pen = property(Arm.getpen, setpen, doc="pen in the current context")
    mode = property(Arm.getmode, setmode, doc="mode in the current context")

    def top_style(self, definition, usage):
        s = f"#{usage['id']}{{"
        
        for pen_usage, (trf, p) in zip(definition.elements, self.repository.references):
            if pen_usage.href.endswith("#Void"):
                continue
            for x in self._translation_attributes[Path]:
                
                value = p[x]
                
                if x is 'transform':
                    try:
                        value = "matrix(%s,%s,%s,%s,%s,%s)"%tuple(trf)
                    except:
                        value = None
                if value is None:
                    continue
                s += f"--{x}-{pen_usage['id']} : {value};"
        s+="}"
        self.styles[usage['id']] = s

    def remove_last_chunk(self):
        return self.stackstack[PenMode].pop()


class Define(SVGBaseEmitter):
    """
    Create definitions
    """
    def __init__(self, *args, **kwargs):
        super(Define, self).__init__(*args, **kwargs)
        self.arm = DefiningArm()
        self.item_placement = self.arm.position()[-1]
        self._instantiating = False
        self.pen_usages = []
        

    
    
    def Canvas(self, cv, *args, **kwargs):
        super(Define, self).Canvas(cv, *args, **kwargs)
        sty = Style()
        #self.svg.add(sty)
        
        ##################
        # Definition step
        ##################
        defined = set()
        for x in cv.commands:
            if x not in defined:
                v = self.define(x)
        

        ##################
        # Instantiation step
        ##################
        
        for x in cv.commands:
            v = self.instantiate(x)
            self.svg.add(v)
            
            
        for x in self.arm.repository.definitions:
            self.svg.defs.add(self.arm.repository.retrieve(x))
            # should it reset anything here?
        self.svg.embed_stylesheet(self.arm.translate_styles())
        self.svg = RemoveVoid().visit(self.svg)

        
    def DrawClose(self, item, *args, **kwargs):
        self.arm.draw_close(self)
        
    def define(self, item):
        return self.visit(item)
    
    def instantiate(self, item):
        self.arm.instantiating()
        self._instantiating = True
        return self.visit(item)
    
    def Spiral(self, item, *args, **kwargs):
        for x in item():
            self.define(x)
        
    def Polygon(self, item, *args, **kwargs):
        for x in item():
            self.define(x)

    def Pen(self, item, *args, **kwargs):
        if self.arm.pen is item:
            return
        super(Define, self).Pen(item, *args, *kwargs)
    
    def NewReference(self, item, *args, **kwargs):
        nr = self.arm.new_reference(item)
        return nr
    
    def SaveLabel(self, item, *args, **kwargs):
        # TODO : Might be good to remember pen mode, too
        
        self.arm.save(item)
        self.arm.new_reference(newref(), with_name=item)
        
    
    def RestoreLabel(self, item, *args, **kwargs):
        # TODO : Might be good to remember pen mode, too
        
        self.arm.restore(item)
        
    def MoveToLabel(self, item, *args, **kwargs):
        self.arm.restore(item)
    
    def DrawTuple(self, item, *args, **kwargs):
        self.arm.open_path()
        self.tuple(item,*args, **kwargs)
    
    def MoveTuple(self, item, *args, **kwargs):
        self.arm.close_path()
        self.tuple(item, *args, **kwargs)
        
    def tuple(self, item, *args, **kwargs):
        cp = self.arm.position() @ (0, 0)
        head = "M"
        partial = (head,) + tuple(cp)

        cgen = self._generator(item)
        
        for (x, y), cmd in zip(pairwise(item), cgen):
            _x, _y = self.arm.move(x, y, self.arm.mode)
            partial += ((cmd,) if cmd else ()) + (_x, _y)
        
        self.arm.add_chunk(partial)
        self.add_axes(self.arm.position().world()[-1])
        #return p, tr

    def Sequence(self, item, *args, **kwargs):
        # self.merger.emit(item)
        
        for x in item:
            self.emit(x)
        if self._instantiating:
            usage_id = "top"
        else:
            usage_id = None
        de, gg = self.arm.group(item, usage_id=usage_id)
        if self._instantiating:
            self.arm.top_style(de, gg)
        try:
            gg.matrix(*self.item_placement)
        except:
            raise
        self.arm.close_path()
        
        last = self.arm.reset_reference_list()
        
        if self._instantiating:
            self.arm.stackstack.clear()
            self.arm.clear_context()
            self.item_placement = last[0]
            
        else:
            
            self.arm.clear()
        
        return gg
        

def _children_(fun):
    return _a_.children(child_iterator=lambda x: x.elements, 
                        children_first=True,
                        aggregate=lambda x: [y for y in x if y is not None])\
        (fun)



class RemoveVoid(Visitor):
    
    def __init__(self):
        super(RemoveVoid, self).__init__()
        self.empty_groups = set()
    
    @_children_
    def Drawing(self,item, *args, **kwargs):
        drw = self._new_item(item, **kwargs)
        drw.elements.pop(0)
        drw.defs = drw.elements[0]
        drw.attribs = item.attribs
        drw.filename = item.filename
        return drw

    def Use(self,item, *args, **kwargs):
        if item.href == "#Void" or item.href[1:] in self.empty_groups:
            return None
        return item
    
    def Path(self,item, *args, **kwargs):
        return item

    @_children_
    def Group(self,item, *args, **kwargs):
        return self._new_item(item, **kwargs)
    
    def _new_item(self, item, **kwargs):
        cr = kwargs['children_result']
        if cr:
            g = type(item)()
            try:
                g['id'] = item['id']
            except KeyError:
                pass
            for x in cr:
                g.add(x)
        else:
            self.empty_groups.add(item['id'])
            g = None
            
        return g
     
    
