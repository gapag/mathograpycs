import svgwrite
from svgwrite.container import Use, Group
from svgwrite.path import Path

from algebra import pairwise
from core import Emitter, DrawTuple

class CommandGenerator:
    def __init__(self, letter, once=False):
        self.letter = letter
        self.once = once

    def __iter__(self):
        once = self.once
        yield self.letter
        once = not once
        while once:
            yield self.letter
        while True:
            yield ''

_path = ['stroke',
               'stroke_width',
               'fill', 'stroke_linecap',
               'stroke_linejoin']
_transform = ['transform']

class _PenUpdate:
    
    svg_args = {
        Path: _path,
        Group : _transform,
        Use : _path+_transform
    }

    args_defaults = {
        'stroke': 'black', # If I want to make efficient use of reuse, should remove this.
        'stroke_width': '1',
        'fill': 'none',
        'transform':None
    }

    def __init__(self, **kwargs):
        super(_PenUpdate, self).__init__()



def move_only_path(d):
    move_chunk = []
    for x in d:
        if x in ('z', 'Z'):
            continue
        elif x in ('m', 'M'):
            move_chunk.clear()
        elif isinstance(x, (int, float)):
            move_chunk.append(x)
            if len(move_chunk) > 2:
                return False
        else:
            return False
    return True

class SVGBaseEmitter(Emitter):
    
    def __init__(self, *args, **kwargs):
        super(SVGBaseEmitter, self).__init__(*args, **kwargs)
        self.defining = False
        self._canvas = None
        group = Group(id="axes")
        Y = Path(d=('M', 0, 0, 'l', 0, 20, 3, -3), stroke="red", id="Y")
        X = Path(d=('M', 0, 0, 'l', 20, 0, -3, 3), stroke="black")
        group.add(X)
        group.add(Y)

        self.debug_axes_proto = group
        self.debug_axes = []
    
    def add_axes(self, mat):
        if self.debug:
            u = Use("#axes", fill=self.arm.pen.stroke)
            u.matrix(*list(mat))
            self.debug_axes.append(u)
    
    
    
    
    def _usage(self, li, base_transform):
        path, mat = self.define.repository.retrieve(self.current,
                                                    self.active_pen, li)
        u = self.use(path, base_transform)
        return path, mat, u

    

    def PenMode(self, item, *args, **kwargs):
        self.arm.mode = item
    
    
    def _generator(self, command):
        once = isinstance(command, DrawTuple)
        letter = 'l' if once else 'm'
        absolute_spec = not self.arm.mode.relative #or self.arm.mode.polar
        letter = letter.upper() if absolute_spec else letter
        return CommandGenerator(letter, once)
    
    defaults = _PenUpdate.args_defaults
    
    def save(self):
        for x in self.debug_axes:
            self.svg.add(x)
        self.svg.save(self.pretty)
        
    def Canvas(self, cv, *args, **kwargs):
        self.__canvas = cv
        wh = (w, h) = (cv.width, cv.height)
        # try:
        #     self.current = next(iter(self.__canvas._pens))
        # except StopIteration:
        #     raise NoPenAvailable()

        self.svg = svgwrite.Drawing(
            filename=self.filename, size=wh,
            viewBox=f"{-w/2} {-h/2} {w} {h}",
            fill="none")
        if self.debug:
            self.svg.defs.add(self.debug_axes_proto)
        # for pen in self.arm._pens:
        #     pen.position.clear()

        