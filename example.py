import math

from algebra import pairwise, cartesian
from core import Canvas, Pen, cart, rcart, path, Mat, Polygon, \
    Spiral, \
    transform, pol, rpol, close, Sequence, newref, restore
from core import reset
from emit.continuous import NoDefinitionsEmitter, ContinuousPath, Define

minchia = Canvas(100, 100)
k=10

its = 50
poli = Polygon(10, 5, star=1)
spi = Spiral(radius_fun=lambda r, a: .95 * math.sqrt(a),
             #divergence_fun=lambda r, a: a+10,
             divergence_fun=Spiral.daisy,
             of=poli,
             iterations=its,
             )

spi_line = Spiral(radius_fun=lambda r, a: 5 * math.sqrt(a),
                  divergence_fun=lambda r, a: a + 10,
                  # divergence_fun=Spiral.daisy,
                  # of= poli ,
                  iterations=its,
                  )


def mixed(first_pen, second_pen):
    return first_pen >> pol() >> (20, 40, 50, 45) >> second_pen >> rcart() >> (
    10, 0)

def two_mixed_new_reference(p,q,r,w):
    return mixed(p, q) >> newref() >> mixed(r, w) >> newref()

def polispi():
    pass

def two_spirals(pen, spiral):
    return pen >> pol() >> spiral << (100, 45) >> newref() >> spiral

def cross(pen):
    abscissa = pen >> cart() >> (-10, 0, 10, 0) << (0,0)
    ordinata = pen >> cart() >> (0, -10, 0, 10) << (0,0)
    return [abscissa , ordinata]


def run_canvas(the_canvas):
    p = the_canvas.pen(name='pen_p', stroke='orange', stroke_width=1)
    q = the_canvas.pen(name='pen_q', stroke='blue', stroke_width=3)
    r = the_canvas.pen(name='pen_r', stroke='red', stroke_width=3)
    w = the_canvas.pen(name='pen_w', stroke='purple', stroke_width=3)
    
    points = [(0,-100), (130,20), (-130, 20)]
    to_paint = [
        # pol() >> p << "sborro" >>  poli << restore("sborro") >> (30, 90) >> "sborro" >> poli << restore("sborro") >> (30, 90) >> "sborro" >> poli << restore("sborro")
        # w >> cart() << (0, -100) >> rpol() >> (100, 45),
        # w >> cart() << (0, -100) >> rpol() >> (100, 3*45),
        # w >> cart() << (130, 20) >> rpol() >> (100, 45),
        # w >> cart() << (130, 20) >> rpol() >> (100, 3 * 45),
        # w >> cart() << (-130, 20) >> rpol() >> (100, 45),
        # w >> cart() << (-130, 20) >> rpol() >> (100, 3 * 45),
        #w >> cart() << (-200, 150) >> rcart() >> (100,-100,100,100),
        q >> cart() >> x for x in points
    ]
    
    s = Sequence() >> r >> cart() << (0,0)
    
    for x in  points:
        for y in [45, 3*45]:
            s >> cart() << x >> rpol() >> (100, y)
    
     
    
    
#     for seq in to_paint:
#         seq in the_canvas
# ##    s in the_canvas
    two_spirals(p, spi_line) in the_canvas
    # aa in the_canvas
    # bb in the_canvas
import traceback
debug = False
try:
    with Canvas(600, 300, emitter=ContinuousPath(f'the_canvas_dio_continuous.svg', debug=debug, pretty=True)) as c2:
        run_canvas(c2)
except:
    traceback.print_exc()
    print("error with continuous")

try:
    with Canvas(500, 300, emitter=Define(f'the_canvas_dio.svg', debug=debug, pretty=True)) as c1:
        run_canvas(c1)
except:
    traceback.print_exc()
    print("error with define")
