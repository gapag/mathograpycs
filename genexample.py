#!/usr/bin/python3
import glob
import subprocess
inject = """

_{name}_

```python
{content}
```

_{name}.svg_

![alt_text](doc/{name}.svg) 

"""
if __name__ == '__main__':
    contents = {}
    for x in glob.glob("./doc/doc*.py"):
        # execute each python script
        subprocess.run(['python3.8', x])
        # read each python script
        with open(x) as fl:
           contents[x[6:-3]] = inject.format(content=fl.read(), name=x[6:])

    with open('README_template.md') as ff:
        with open('README.md', 'w') as fw:
            fw.write(ff.read().format(**contents))
        # replace things here
