from setuptools import setup, find_packages

setup(
    name='mathograpycs',
    version='0.0.1',
    description='A collection of drawing directives following ',
    packages=find_packages(),
    install_requires = [
        "vysitors>=0.0.1"
    ]
)