from collections import OrderedDict

from algebra import TransformStack


class StackStack:
    def __init__(self, hierarchy, hook=None):
        self._data = {x:[] for x in hierarchy}
        self._indexes = {x:e for e,x in enumerate(hierarchy)}
        self._hierarchy = hierarchy[:]
        self.hook = hook or (lambda ty, indx, value, pos, oo : value)

    def push(self, o):
        if o in self._indexes:
            return self.level(o)
        else:
            self._data[self._hierarchy[0]].append(o)
        
    def level(self, o, instance=None):
        idx = self._indexes[o]+1
        val = ()
        
        
        for lvl in self._hierarchy[:idx]:
            
            # concatenate val to self._data[lvl][-1]
            # take the content of level lvl
            # make it a tuple
            
            
            self._data[lvl].append(val)
            
            val = tuple(self._data[lvl])
            #print("val became:",lvl, val)
            self.hook( lvl, idx, val, o, instance)
            # clear level lvl
            self._data[lvl].clear()
        
        nv = self._data[self._hierarchy[idx]]
        nv.append(val)
            
            
        
        return val
        
    def __str__(self):
        s = ""
        for k,x in self._data.items():
            s+=f"{k}:{x}\n"
        return s
    
    def __getitem__(self, item):
        return self._data[item]
        
    def clear(self):
        for x in self._data.values():
            x.clear()

if __name__ == "__main__":
    ss = StackStack("lsepc")
    vv = StackStack("lsepc")
    sv = (0, 'e', 'p')
    v = (0,'s','e','p')
    for x in sv:
         ss.push(x)
         print(sv, ss)
    print("#"*100)
    for x in v:
        vv.push(x)
        print(v, vv)
        
    
    
    print("sabelli")
    
    